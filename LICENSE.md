Like everything SFS makes, unless otherwise specified, these class materials are copyleft.

You may:
- **Use** them, for whatever purpose you see fit
- **Study** them, to understand how they work
- **Copy** and share them with your friends
- **Modify** them to better suit your needs

IIF:
- You credit us for our work
- You grant your learners these same freedoms

Code is GPL

Images and Words are CC BY SA
