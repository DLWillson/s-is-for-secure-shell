Set up a remote network shaped like this:

```
Internet
   |
------- public
   |
   LB
   |
------- service
 | | |
 A A A
 | | |
------- storage
   |
   DB
```

- LB runs sshd and haproxy or nginx, round-robins to As
- As run sshd and httpd or nginx, return host-unique web-pages
- DB runs Postgres or MariaDB

Install osadmin on all machines using these files and scripts here. osadmin should be able create learners' user accounts, but nothing else.
